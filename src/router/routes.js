export default [
  {
    path: '/nutoniko',
    component: () => import( /* webpackChunkName: "index" */ 'pages/Index'),
    children: [
      {
        path: '',
        component: () => import( /* webpackChunkName: "vote-list" */ 'pages/VoteList'),
      },
      {
        path: '*',
        component: () => import( /* webpackChunkName: "not-found" */ 'pages/NotFound'),
      },
    ]
  },
  { 
    path: '/',
    redirect: '/nutoniko'
  },
]